# SQLson

## What is

__SQLson__ is a simple library for converting sqlson standard to `select` sql query format. when you need a customizable loader filter you must send your query to server and load data, so __SQLson__ can make it easy.

## How is

Working with sqlson is very simple for installing and using you can follow these steps:

### Installation

You can install and add sqlson to your node package using this command

```bash
npm install --save sqlson
```

### Usage

For more information about __SQLson__ you can see [Interface](https://gitlab.com/ckoliber/SQLson/blob/master/docs/docs/INTERFACE.md)
