# How to start a standard project

> For creating a standard project based on laws we have these suggested levels.

1. __Base configuring__
2. __Documenting__
    1. START.md
    2. LICENSE.md
    3. HOWTO.md
    4. STANDARD.md
    5. TODO.md
    6. INTERFACE.md
    7. [PROTOCOL.md ]
3. __Base coding__
4. __Refactoring documentations__ (until end)
5. __Refactoring codes__ (until end)
    1. Naming
    2. Commenting
    3. Spacing
    4. Performance refactoring
6. __Git using__ (until end)
    1. Stage
    2. Commit
    3. Push
    4. Pull
    5. Branching
    6. Tagging
________________________

## Base configuring
> This level contains creating base level `directory structures` and configuring `git` and `mkdocs` 

### directory structure

[Details](STANDARD/#project)

### git
> For configuring git we have three level

#### config
```
git init

git config user.name "{your name}"
git config user.email "{your email}"
git config core.editor code
git config core.autocrlf input 
git config credential.helper cache [{timout seconds}]
[git config --unset credential.helper]

git remote add {remote name} [{.git url}]
git push -u {remote name} --all
git push -u {remote name} --tags
git push -u {remote name} master
```

#### .gitattributes
```
* text=auto

*.c text
*.h text
*.js text
*.jsx text
*.md text

*.png binary
*.jpg binary
*.mov binary
```

#### .gitignore
```
.idea
.vscode
source/*/node_modules/
source/*/build/
```

#### .gitmodules
```
```

### mkdocs
> For configuring mkdocs we have three level

#### config
```
sudo apt install python-pip
sudo pip install mkdocs mkdocs-material
cd {project name}
mkdocs new docs
```

#### docs/mkdocs.yml
```
site_name: {project name}
pages:
  - 'Getting Started': START.md
  - 'License': LICENSE.md
  - 'How to start project': HOWTO.md
  - 'Coding Standards': STANDARD.md
  - 'Todo list': TODO.md
  - 'Application Interface': INTERFACE.md
  - 'Application Protocol': PROTOCOL.md
theme: 'material'
```

#### docs/docs/...
> Our markdown documentation files goes here
________________________

## Documenting
> This level contains documenting level for keeping clear in coding length

### START.md
> At the `START.md` we will writing project descriptions such as these items

* __Author__ (project author or manager details)
* __Developers__ (project developers details)
* __Date__ (project start date)
* __Description__ (project description)
* __Goal__ (project goal)
* __License__ (project license)
* __Languages__ (project languages)
* __Dependencies__ (project dependencies)
* __Build__ (project building levels)

### LICENSE.md
> At the `LICENSE.md` we will writing project license description

### HOWTO.md
> At the `HOWTO.md` we will writing project creation standards and levels

### STANDARD.md
> At the `STANDARD.md` we will writing project coding standards such as these items

* __Tips__ (some basic tips)
* __Directory structuring__ (project directory system)
* __Folder, File(Module), Namespace, Class,  Method, Variable, Identifier, Import naming__ (project naming system)
* __Commenting__ (project commenting system)
* __Spacing__ (project spacing system)

### TODO.md
> At the `TODO.md` we will writing project futures and tagging them and deadlines
>> For writing todo's we can use these rules

1. __Contents__
    * __Example__
        1. __user__ -> login, logout, state
        2. __map__ -> search, set, get, analyze
        3. __search__ -> site search
        4. __aboutus__ -> about us
        5. __contactus__ -> contact us
        6. __content__ -> site content and posts
2. __Parts__
    * __Example__
        1. __indexpage__ 
            1. signin dialog
            2. signout dialog
            3. signup dialog
            4. aboutus dialog
            5. contactus dialog
            6. toolbar
            7. carousel
            8. futures
            9. description
            10. footer
            11. fab
        2. __mappage__
            1. signin dialog
            2. signout dialog
            3. signup dialog
            4. aboutus dialog
            5. contactus dialog
            6. toolbar
            7. map
            8. fab
        3. __adminpage__
            * ...
        4. __contentpage__
            * ...
        5. __detailpage__
            * ...
3. __Tagging__
    > We should tag our futures __until the next big release__
    * __Example__
        1. __indexpage__  -> 0.1.Z
            1. signin dialog -> 0.1.Z
            2. signout dialog -> 0.1.Z
            3. signup dialog -> 0.1.Z
            4. aboutus dialog -> 0.1.Z
            5. contactus dialog -> 0.1.Z
            6. toolbar -> 0.1.Z
            7. carousel -> 0.1.Z
            8. futures -> 0.1.Z
            9. description -> 0.1.Z
            10. footer -> 0.1.Z
            11. fab -> 0.1.Z
        2. __mappage__ -> 0.1.Z
            1. signin dialog -> 0.1.Z
            2. signout dialog -> 0.1.Z
            3. signup dialog -> 0.1.Z
            4. aboutus dialog -> 0.1.Z
            5. contactus dialog -> 0.1.Z
            6. toolbar -> 0.1.Z
            7. map -> 0.1.Z
            8. fab -> 0.1.Z
        3. __adminpage__ -> 0.3.Z
            * ...
        4. __contentpage__ -> 0.4.Z
            * ...
        5. __detailpage__ -> 0.4.Z
            * ...
4. __Styling Views__
    > In this level we should design our `mockup` using application or in a paper

### INTERFACE.md
> At the `INTERFACE.md` we will writing project api interface and params and return values and descriptions of api calls
>> This Api may be a `socket`, `RESTFul`, `websocket`, etc

### PROTOCOL.md
> At the `PROTOCOL.md` we will writing project protocols and models descriptions and diagrams and explain all of them in two level: `General`, `Specific`
>> This file should contains two basic and important part: `DBMS Model`, `Application Model`

1. __DBMS Model__:
    Per `source` if contains DBMS (Example: server side MySQL, android: SQLite, ...) we have these parts

    1. __Explaintion__:
        Explain __Databases__ and __Tables__ of them, per table explain __Columns__
    2. __Diagram__:
        Show One Big __DataSet__ package that contains __Databases__ that contains __Tables__ that contains __Columns__

    Example:
```
## DBMS Model
### `node` Model
#### Explaintion
...(Databases, Tables, Columns description and goal)

#### Diagram
...(DataSet diagram)

### `react` Model
#### Explaintion
...(Databases, Tables, Columns description and goal)

#### Diagram
...(DataSet diagram)
``` 

2. __Application Model__
    We have a `General BPMN` (with out any sub-process or task, describes general and global application model contains every `source` in one BPMN) and per `source` we have a model

    Example:
```
## Application Model
### General BPMN
...(General BPMN)

### `node` BPMN
...(`node` BPMN)

### `react` BPMN
...(`react` BPMN)
```

________________________

## Base coding
> This level contains base coding level in this level we will start the project coding with out focusing on standards of codings
________________________

## Refactoring documentations
> This level contains refactoring documentations level in this level that is a loop until end of project if we needed , we will add somethings to our documentations or edit it
________________________

## Refactoring codes
> This level contains refactoring codes level in this level that is a loop until end of project we will refactoring our codes based on our standards at every periods
________________________

## Git using
> This level contains git using level in this level that is a loop until end of project we will use git in our project for version controlling

### Stage, Commit (To local repository)
> At every quick editing or coding of our project we should save our changes or commit theme
>> For example every 10 min we should commit at least one stage to our local repository

### Push (To remote repository)
> At every day editing after coding before release theme we should save our codes in a stable repository such as `gitlab.com` remote repository
>> For example every day we should push at least one commit to our remote repository

### Pull (From remote repository)
> When ever we coding on two different system or need to rebase our codes from remote repository or etc, we should `pull` and `stage` and `commit` from and to remote and local repositories

### Branching
> Managing branches is an important part of group working, creating standard branches and merging theme based on these rules

#### Branches names
##### master (master branch)
> In `master` branch we `tagging` the code 
>> Bacause of global bug fixing we dont create a different branch such as `hotfix` for our bug fixing releases, so all of the releases are going into `master` branch with full clear tag

##### develop
> In `develop` branch we put our before release codes for final testing and analyzing and profiling if result was successful we merge `develop` branch into `master` and then tag it

##### develop\_{developer name}
> Every developer has it's own fork of code and can `rebase` it from master, after completing code `test`, `analyze` by self then will merge it into `develop` and from `develop` after final testing merge it into `master` and tag it, then if other developers needed the new release they can `rebase` them codes from `master`

### Tagging
> Managing tags is an important part of releasing project, at the `release` branch we will tag our code versions based on these rules

#### X.Y.Z

##### X
> Shows the big release number

##### Y
> Shows the futures that added to this `X` version

##### Z
> Shows the bugs that fixed from this `X` version

##### Example
* __0.1.0__ (start version) 
* __1.2.1__
* __2.2.0__
* __0.5.34__
________________________

