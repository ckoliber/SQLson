# Getting Started

Now we are going to decribe the project

## Author

SQLson author is [KoLiBer](mailto://koliberr136a1@gmail.com).

## Developers 
1. [KoLiBer](mailto://koliberr136a1@gmail.com)
2. [KoLiBer](mailto://koliberr136a1@gmail.com)
3. [KoLiBer](mailto://koliberr136a1@gmail.com)
4. [KoLiBer](mailto://koliberr136a1@gmail.com)

## Date

SQLson started at __Tuesday, March 13, 2018__ UTC date.

## Description

SQLson is a library for converting json format to sql select query format.

## License

SQLson is under __MIT__ license. more informations about __MIT License__ is [here](LICENSE.md)
![MIT License](https://pre00.deviantart.net/4938/th/pre/f/2016/070/3/b/mit_license_logo_by_excaliburzero-d9ur2lg.png)

## Languages

1. [HTML5]()
2. [CSS3]()
3. [JavaScript-ES6]()
4. [Node.JS]()
    
## Dependencies

1. [Git]()
2. [Docker]()
3. [Python]()
4. [Node.JS]()
5. [Python]()

## Build

 