exports.sqlson = {

    // method: text to sqlson
    parse: (text) => {
        return {};
    },

    // method: sqlson to text
    sqlify: (value) => {

        // method: sqlify select block
        const sqlify_select = (object) => {
            if ('ARRAY' in object && 'FROM' in object) {
                let array = [];
                for (let column of object.ARRAY) {
                    if ('COLUMN' in column) {
                        if ('AS' in column) {
                            array.push(`${column.COLUMN} AS ${column.AS}`);
                        } else {
                            array.push(`${column.COLUMN}`);
                        }
                    } else {
                        return undefined;
                    }
                }
                return `${array.join(",")} FROM ${object.FROM}`;
            } else {
                return undefined;
            }
        }

        // method: sqlify where block
        const sqlify_where = (object) => {
            if ('COLUMN' in object && 'OPERATOR' in object) {
                if (object.OPERATOR === "=" && 'VALUE' in object) {
                    return `(${object.COLUMN}='${object.VALUE}')`;
                } else if (object.OPERATOR === "<>" && 'VALUE' in object) {
                    return `(${object.COLUMN}<>'${object.VALUE}')`;
                } else if (object.OPERATOR === ">" && 'VALUE' in object) {
                    return `(${object.COLUMN}>'${object.VALUE}')`;
                } else if (object.OPERATOR === "<" && 'VALUE' in object) {
                    return `(${object.COLUMN}<'${object.VALUE}')`;
                } else if (object.OPERATOR === ">=" && 'VALUE' in object) {
                    return `(${object.COLUMN}>='${object.VALUE}')`;
                } else if (object.OPERATOR === "<=" && 'VALUE' in object) {
                    return `(${object.COLUMN}<='${object.VALUE}')`;
                } else if (object.OPERATOR === "BETWEEN" && 'FROM' in object && 'TO' in object) {
                    if (object.FROM <= object.TO) {
                        return `(${object.COLUMN} BETWEEN '${object.FROM}' AND '${object.TO}')`;
                    } else {
                        return `(${object.COLUMN} BETWEEN '${object.TO}' AND '${object.FROM}')`;
                    }
                } else if (object.OPERATOR === "LIKE" && 'PATTERN' in object) {
                    return `(${object.COLUMN} LIKE '${object.PATTERN}')`;
                } else if (object.OPERATOR === "IN" && 'ARRAY' in object) {
                    return `(${object.COLUMN} IN ('${object.ARRAY.map((object) => `'${object}'`).join(",")}'))`;
                } else if (object.OPERATOR === "IN" && 'SQLSON' in object) {
                    return `(${object.COLUMN} IN (${sqlify(object.SQLSON)}))`;
                } else {
                    return undefined;
                }
            } else if ('NOT' in object) {
                let text = sqlify_where(object.NOT);
                text = (text === "()") ? "(false)" : text;
                if (text) {
                    return `(NOT ${text})`;
                } else {
                    return undefined;
                }
            } else if ('AND' in object || 'OR' in object) {
                let array = [];
                for (let text of (object.AND || object.OR)) {
                    let text_inner = sqlify_where(text);
                    text_inner = (text_inner === "()") ? ('AND' in object ? "(true)" : "(false)") : text_inner;
                    if (text_inner) {
                        array.push(text_inner);
                    } else {
                        return undefined;
                    }
                }
                return `(${array.join(object.AND ? " AND " : " OR ")})`;
            } else {
                return undefined;
            }
        }

        // method: sqlify group by block
        const sqlify_group = (object) => {
            if ('ARRAY' in object) {
                return `${object.ARRAY.join(",")}`;
            } else {
                return undefined;
            }
        }

        // method: sqlify having block
        const sqlify_having = (object) => {
            return sqlify_where(object);
        }

        // method: sqlify order by block
        const sqlify_order = (object) => {
            if ('ARRAY' in object && 'ORDER' in object && (object.ORDER === "ASC" || object.ORDER === "DESC")) {
                return `${object.ARRAY.join(",")} ${object.ORDER}`;
            } else {
                return undefined;
            }
        }

        // method: sqlify limit and offset block
        const sqlify_limit = (object) => {
            if ('LIMIT' in object) {
                if ('OFFSET' in object) {
                    return `${object.LIMIT}`;
                } else {
                    return `${object.LIMIT} OFFSET ${object.OFFSET}`;
                }
            } else {
                return undefined;
            }
        }

        let select = "";
        if ('SELECT' in value) {
            let select_value = sqlify_select(value.SELECT);
            if (select_value) {
                select = `SELECT ${select_value} `;
            } else {
                return undefined;
            }
        }

        let where = "";
        if ('WHERE' in value) {
            let where_value = sqlify_where(value.WHERE);
            if (where_value) {
                where = `WHERE ${where_value} `;
            } else {
                return undefined;
            }
        }

        let group = "";
        if ('GROUP' in value) {
            let group_value = sqlify_group(value.GROUP);
            if (group_value) {
                group = `GROUP BY ${group_value} `;
            } else {
                return undefined;
            }
        }

        let having = "";
        if ('HAVING' in value) {
            let having_value = sqlify_having(value.HAVING);
            if (having_value) {
                having = `HAVING ${having_value} `;
            } else {
                return undefined;
            }
        }

        let order = "";
        if ('ORDER' in value) {
            let order_value = sqlify_order(value.ORDER);
            if (order_value) {
                order = `ORDER BY ${order_value} `;
            } else {
                return undefined;
            }
        }

        let limit = "";
        if ('LIMIT' in value) {
            let limit_value = sqlify_limit(value.LIMIT);
            if (limit_value) {
                limit = `LIMIT ${limit_value}`;
            } else {
                return undefined;
            }
        }

        return select + where + group + having + order + limit;
    }

};